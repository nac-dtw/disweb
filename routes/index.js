var express = require('express');
var router = express.Router();
var swig = require('swig');


function Anuncio (descripcion, precio) {
  this.descripcion = descripcion;
  this.precio = precio;
}

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/', function (req, res) {
  res.send('Petición respondida');
})


router.get('/anuncios', function (req, res) {
  var pro1 = new Anuncio("Nokia 230",56);
  var pro2 = new Anuncio("Nokia 730",156);
  var pro3 = new Anuncio("Nokia 635",173);
  var anuncios = [pro1,pro2,pro3];

  var respuesta = swig.renderFile('public/anuncios.html', {
    vendedor: 'Jordan',
    anuncios: anuncios
  });
  res.send(respuesta);
})

router.get('/anuncio/:id/detalles/', function (req, res) {
  res.send('Detalles anuncio:'+req.params.id);
})

router.post('/anuncios', function (req, res) {
  res.send('Anuncios');
})

router.post('/anuncio', function (req, res) {
  res.send('Anuncio Guardado:'+req.body.descripcion
      +" precio: "+req.body.precio);
})

module.exports = router;
